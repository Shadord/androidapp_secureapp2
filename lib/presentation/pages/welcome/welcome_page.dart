import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:password_secure/presentation/pages/auth/auth_controller.dart';


class WelcomePage extends StatelessWidget {
  double currentPage = 0.0;
  final _pageViewController = new PageController();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(
        init: AuthController(),
        builder: (controller) {
          return Scaffold(
              backgroundColor: Colors.blue[400],
              body: Container(
                child: Stack(
                  children: <Widget>[
                    PageView(
                      controller: _pageViewController,
                      children: [
                        Container(child: Text("Hello"),),
                        Container(child: Text("Hello"),),
                        Container(child: Text("Hello"),),
                        Container(child: Text("Hello"),),
                      ],
                    ),
                  ],
                ),
              ),
          );
        });
  }
}
