
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:crypto/crypto.dart';
import 'package:get_storage/get_storage.dart';
import 'package:password_secure/presentation/routes/app_pages.dart';

class AuthController extends GetxController {
  RxString authPass = new RxString();
  RxString authPass2 = new RxString();
  RxString mainText = new RxString("Enter your first password");
  final box = GetStorage();


  AuthController() {
    this.authPass.value = "";
    this.authPass2.value = "";

  }

  checkPassword() {
    return box.hasData("4722515aad80071");

  }

  verifyAuth() {
    if(checkPassword()) {
      var hash = passwordSHA(this.authPass.value);
      print("Hashed = ${hash}");
      String pass = box.read("4722515aad80071");
      if(hash == pass){
        Get.offAndToNamed(Routes.HOME);
      }
    }else if(authPass2.value == ""){
      authPass2.value = authPass.value;
      authPass.value = "";
      mainText.value = "Enter the password twice";
    }else if(authPass2.value != "") {
      if(authPass2.value == authPass.value) { // OK
        String hash = passwordSHA(this.authPass.value);
        box.write("4722515aad80071", hash);
        Get.offAndToNamed(Routes.HOME);
      }else{
        authPass.value = "";
        authPass2.value = "";
        mainText.value = "Error : Enter your first password";
      }
    }
  }

  passwordSHA(String pass) {
    var byte = utf8.encode(pass);
    var hashed = sha256.convert(byte);
    return hashed.toString();
  }
}