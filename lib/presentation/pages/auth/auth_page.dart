import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:password_secure/presentation/pages/auth/auth_controller.dart';


class AuthPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(
        init: AuthController(),
        builder: (controller) {
          return Scaffold(
              backgroundColor: Colors.blue[400],
              body: Container(
                margin: EdgeInsets.only(left: 70, right: 70, top: 70),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    controller.checkPassword() ? Text("") : Obx(() => Text(controller.mainText.value)),
                    Obx(() =>TextFormField(
                      textAlign: TextAlign.center,
                      textAlignVertical: TextAlignVertical.center,
                      readOnly: true,
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        hintText: controller.authPass.value,
                        hintStyle: TextStyle(
                          fontSize: 25,
                          color: Colors.white
                        )
                      ),
                    )),
                  GridView(
                    scrollDirection: Axis.vertical,           //default
                    reverse: false,                           //default
                    controller: ScrollController(),
                    primary: false,
                    shrinkWrap: true,
                    padding: EdgeInsets.all(5.0),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisSpacing: 5.0,
                    crossAxisSpacing: 5.0,
                  ),
                  clipBehavior: Clip.hardEdge,
                  keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.manual,
                  children: [
                    button(controller, "1"),
                    button(controller, "2"),
                    button(controller, "3"),
                    button(controller, "4"),
                    button(controller, "5"),
                    button(controller, "6"),
                    button(controller, "7"),
                    button(controller, "8"),
                    button(controller, "9"),
                    button(controller, "<-"),
                    button(controller, "0"),
                    button(controller, "Enter"),]                     // List of Widgets
                ),

                  ],
                ),
              )
          );
        });
  }

  Widget button(AuthController ac, String value) {
    return FlatButton(
      child: Text(value),
      onPressed: () {
        if(ac.authPass.value.length < 8 && value != "<-" && value != "Enter")
          ac.authPass.value += value;
        if(value == "<-" && ac.authPass.value.length > 0)
          ac.authPass.value = ac.authPass.value.substring(0, ac.authPass.value.length-1);
        if(value == "Enter" && ac.authPass.value.length > 3)
          ac.verifyAuth();
      },
      color: Colors.blue,
      textColor: Colors.white,
    );
  }



}
