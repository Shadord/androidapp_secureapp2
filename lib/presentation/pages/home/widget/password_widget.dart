
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:password_secure/application/passwords/password_model.dart';
import 'package:password_secure/presentation/shared/snackbar.dart';
import 'package:rounded_letter/rounded_letter.dart';
import 'package:flutter/services.dart';

class PasswordWidget extends StatelessWidget {
  final Password password;
  PasswordWidget({@required this.password});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 12, horizontal: 10),
      child: ListTile(
        leading: RoundedLetter(text: this.password.name.substring(0, 1), fontSize: 25, shapeSize: 50, shapeColor: this.password.getPasswordColor(),),
        title: Text(this.password.name),
        subtitle: Text(this.password.url),
        onTap: () {
          ClipboardData c = new ClipboardData(text: this.password.value);
          Clipboard.setData(c);
          PasswordSnackBar("Copied", "${this.password.name} password has been copied to clipboard.").show();
        },

      ),
    );
  }
}