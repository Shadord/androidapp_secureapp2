import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:password_secure/application/passwords/password_controller.dart';
import 'package:password_secure/application/passwords/password_model.dart';
import 'package:password_secure/presentation/pages/home/widget/password_widget.dart';
import 'package:password_secure/presentation/routes/app_pages.dart';

class HomePage extends StatelessWidget {
  final PasswordController passwordController = Get.find();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PasswordController>(
        init: PasswordController(),
        id: "Home",
        builder: (controller) {
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.blue,
              title: Text("Passwords"),
              actions: [
                IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    Get.snackbar("Pressed", "Search",
                        snackPosition: SnackPosition.BOTTOM,
                        margin: EdgeInsets.symmetric(
                            vertical: 20,
                            horizontal: 10
                        )
                    );
                  },
                ),
                IconButton(
                  icon: Icon(Icons.attach_file),
                  onPressed: () {
                    Get.snackbar("Pressed", "Documents",
                        snackPosition: SnackPosition.BOTTOM,
                        margin: EdgeInsets.symmetric(
                            vertical: 20,
                            horizontal: 10
                        ));
                  },
                ),
                IconButton(
                  icon: Icon(Icons.settings),
                  onPressed: () {
                    Get.snackbar(
                        "Pressed", "Settings",
                        snackPosition: SnackPosition.BOTTOM,
                        icon: Icon(Icons.map),
                        margin: EdgeInsets.symmetric(
                            vertical: 20,
                            horizontal: 10
                        )

                    );
                  },
                )
              ],
            ),
            body: Obx(
                  () =>
                  ListView.builder(
                    itemCount: passwordController.passwordList.value.length,
                    itemBuilder: (context, index) {
                      final Password password = passwordController.passwordList
                          .value[index];
                      print("ItemB = " +
                          passwordController.passwordList.toString());
                      return Dismissible(
                          key: UniqueKey(),
                          direction: DismissDirection.endToStart,
                          onDismissed: (_) {
                            passwordController.removePassword(index);
                            print("Dismiss = " +
                                passwordController.passwordList.toString());
                            Get.snackbar(
                                "Removed", "${password.name} has been removed",
                                snackPosition: SnackPosition.BOTTOM,
                                barBlur: 100,
                                mainButton: FlatButton(
                                  child: Text("Undo"),
                                  textColor: Colors.amber[700],
                                  onPressed: () {
                                    print(password.name);
                                    passwordController.insertPassword(
                                        index, password);
                                  },
                                ),
                                margin: EdgeInsets.symmetric(
                                    vertical: 20,
                                    horizontal: 10
                                ));
                          },
                          child: PasswordWidget(password: password)
                      );
                    },
                  ),
            ),
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                Get.toNamed(Routes.NEW_PASSWORD, parameters: {"name": "Hello"});
              },
            ),
          );
        });
  }
}
