
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:password_secure/application/passwords/password_model.dart';

class NewPasswordController extends GetxController {
  RxString passwordName = new RxString();
  RxString passwordURL = new RxString();
  RxString passwordValue = new RxString();
  RxDouble passwordStrength = new RxDouble();
  RxInt passwordCategory = new RxInt();

  NewPasswordController() {
    this.passwordStrength.value = 0;
  }

  getPassword() {
    return new Password(name: this.passwordName.value, url: this.passwordURL.value, value: this.passwordValue.value, category: this.passwordCategory.value);
  }

  setPasswordCategory(value) {
    this.passwordCategory.value = value;
  }

  valueCalculation(String password) {
    //total score of password
    double iPasswordScore = 0;

    if( password.length < 8 )
      return 0;
    else if( password.length >= 10 )
      iPasswordScore += 2;
    else
      iPasswordScore += 1;

    //if it contains one digit, add 2 to total score
    if( password.contains("(?=.*[0-9]).*") )
      iPasswordScore += 2;

    //if it contains one lower case letter, add 2 to total score
    if( password.contains("(?=.*[a-z]).*") )
      iPasswordScore += 2;

    //if it contains one upper case letter, add 2 to total score
    if( password.contains("(?=.*[A-Z]).*") )
      iPasswordScore += 2;

    //if it contains one special character, add 2 to total score
    if( password.contains("(?=.*[~!@#\$%^&*()_-]).*") )
      iPasswordScore += 2;
    passwordStrength.value = iPasswordScore*10;
  }


}