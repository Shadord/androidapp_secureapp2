import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:password_secure/application/passwords/password_controller.dart';
import 'package:password_secure/application/passwords/password_model.dart';
import 'package:password_secure/presentation/core/theme/app_colors.dart';
import 'package:password_secure/presentation/pages/new_password/new_password_controller.dart';
import 'package:password_secure/presentation/pages/new_password/widget/password_strength.dart';
import 'package:password_secure/presentation/routes/app_pages.dart';

class NewPasswordPage extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();
  final PasswordController passwordController = Get.find();
  final String name;

  NewPasswordPage({this.name}) {
    if(this.name != null)
      print("Enter new password page " + this.name);

  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NewPasswordController>(
        init: NewPasswordController(),
        builder: (controller) {
          return Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Colors.blue,
                title: Text("New Password"),
                leading: GestureDetector(
                  child: Icon(Icons.arrow_back),
                  onTap: () {
                    Get.back();
                  },
                ),
              ),
              body: SingleChildScrollView(
                child: FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.always,
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FormBuilderTextField(
                            name: 'name',
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Nom",
                            ),
                            onChanged: (text) {
                              controller.passwordName.value = text;
                            },
                            // valueTransformer: (text) => num.tryParse(text),
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(context, errorText: "Ne peut être vide"),
                            ]),
                            keyboardType: TextInputType.name
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            obscureText: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'URL',

                            ),
                            onChanged: (text) {
                              controller.passwordURL.value = text;
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            obscureText: false,
                            validator: (text) {
                              if (text.isEmpty)
                                return "Il faut un mot de passe !";
                              else
                                return null;
                            },
                            onChanged: (text) {
                              controller.valueCalculation(text);
                              controller.passwordValue.value = text;
                            },

                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Password',
                            ),
                          ),
                        ),
                        /*Obx(() { return SizedBox(
                            width: 20,
                            child: PasswordStrength(controller.passwordStrength.value));
                        }),*/
                        Obx(() => Center(
                          child: Wrap(
                            spacing: 6.0,
                            runSpacing: 6.0,
                            children: <Widget>[
                              _buildChip(controller, 'Perso', PasswordCategoryColor.perso, 0),
                              _buildChip(controller, 'Pro', PasswordCategoryColor.pro, 1),
                              _buildChip(controller, 'Gaming', PasswordCategoryColor.gaming, 2),
                            ],
                          ),
                        )),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: MaterialButton(
                            child: Text("Ajouter"),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            textColor: Colors.white,
                            color: Colors.blue,
                            height: 45,
                            minWidth: 800,
                            onPressed: () {
                              passwordController.addPassword(controller.getPassword());
                              Get.back();
                            },
                          ),
                        ),

                      ],
                    ),
                  ),
                ),
              ));
        });
  }

  Widget _buildChip(NewPasswordController npc, String label, Color color, int value) {
    return InputChip(
      label: Text(label),
      selected: npc.passwordCategory.value == value,
      selectedColor: color,
      onSelected: (bool selected) {
        npc.setPasswordCategory(value);
      },
      backgroundColor: color,
      labelStyle: TextStyle(color: Colors.white),
    );
  }


}
