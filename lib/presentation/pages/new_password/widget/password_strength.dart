import 'package:flutter/material.dart';
import 'package:gradient_widgets/gradient_widgets.dart';


class PasswordStrength extends StatelessWidget {
  double _value = 0;

  PasswordStrength(this._value);
  @override
  Widget build(BuildContext context) {
    return GradientProgressIndicator(
      gradient: LinearGradient(colors: [Colors.red,Colors.green]),
      value: this._value,
    );
  }
}

