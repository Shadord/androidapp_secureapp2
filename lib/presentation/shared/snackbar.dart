

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
class PasswordSnackBar {
  final String title;
  final String subtitle;

  PasswordSnackBar(this.title, this.subtitle);

  show() {
    Get.snackbar(this.title, this.subtitle,
      snackPosition: SnackPosition.BOTTOM,
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
    );
  }
}