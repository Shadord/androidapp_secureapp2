import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:password_secure/application/passwords/password_controller.dart';
import 'package:password_secure/presentation/core/theme/app_colors.dart';
import 'package:password_secure/presentation/core/theme/app_theme.dart';
import 'package:password_secure/presentation/core/translations/app_translations.dart';
import 'package:password_secure/presentation/routes/app_pages.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Get.put(PasswordController());
    return GetMaterialApp(
      title: 'Secure',
      theme: appThemeData,
      enableLog: true,
      debugShowCheckedModeBanner: false,
      color: Color(accentColor),
      locale: Locale('fr'),
      fallbackLocale: Locale('fr', 'FR'),
      translationsKeys: AppTranslation.translations,
      navigatorKey: Get.key,
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    );
  }
}