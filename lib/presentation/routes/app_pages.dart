import 'package:get/get.dart';
import 'package:password_secure/presentation/pages/auth/auth_page.dart';
import 'package:password_secure/presentation/pages/home/home_page.dart';
import 'package:password_secure/presentation/pages/new_password/new_password_page.dart';
import 'package:password_secure/presentation/pages/welcome/welcome_page.dart';
part 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.AUTH;

  static final routes = [
    GetPage(
      name: Routes.HOME,
      page: () => HomePage(),
    ),
    GetPage(
      name: Routes.NEW_PASSWORD,
      page: () => NewPasswordPage(),
    ),
    GetPage(
      name: Routes.AUTH,
      page: () => AuthPage(),
    ),
    GetPage(
      name: Routes.WELCOME,
      page: () => WelcomePage(),
    )

  ];
}
