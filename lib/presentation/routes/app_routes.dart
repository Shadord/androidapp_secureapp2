part of 'app_pages.dart';

abstract class Routes {
  static const SPLASH = '/';
  static const AUTH = '/auth';
  static const HOME = '/home';
  static const NEW_PASSWORD = '/new_password';
  static const WELCOME = '/welcome';
}