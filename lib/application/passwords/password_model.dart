
import 'package:flutter/material.dart';
class Password {
  int id;
  String name;
  String url;
  String value;
  int category;

  Password({this.id, this.name, this.url, this.value, this.category});

  setId(int id) {
    this.id = id;
  }

  getPasswordColor() {
    switch(this.category) {
      case PasswordCategory.perso:
        return PasswordCategoryColor.perso;
      case PasswordCategory.pro:
        return PasswordCategoryColor.pro;
      case PasswordCategory.gaming:
        return PasswordCategoryColor.gaming;
    }
  }


}

class PasswordCategory {
  static const int perso = 0;
  static const int pro = 1;
  static const int gaming = 2;
}

class PasswordCategoryColor {
  static Color perso = Colors.green[700];
  static Color pro = Colors.red[600];
  static Color gaming = Colors.orange;
}