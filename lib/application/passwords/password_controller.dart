import 'package:get/get.dart';
import 'package:password_secure/application/passwords/password_model.dart';



class PasswordController extends GetxController {
  Rx<List<Password>> passwordList = Rx<List<Password>>();

  PasswordController();

  @override
  void onInit() => fetchDataFromApi();

  Future<void> fetchDataFromApi() async {
    try {
      final List<Password> data = List<Password>();
      data.add(Password(id:1, name:"Facebook", url:"www.facebook.com", value: "Azertyuiop", category: PasswordCategory.perso));
      data.add(Password(id:2, name:"Outlook", url:"www.outlook.com", value: "Qsdfghjklm", category: PasswordCategory.perso));
      data.add(Password(id:3, name:"Gmail", url:"www.gmail.com", value: "Poiuytreza", category: PasswordCategory.pro));
      data.add(Password(id:4, name:"Steam", url:"www.steam.com", value: "Mlkjhgfdsq", category: PasswordCategory.gaming));
      data.add(Password(id:1, name:"Facebook", url:"www.facebook.com", value: "Azertyuiop", category: PasswordCategory.perso));
      data.add(Password(id:2, name:"Outlook", url:"www.outlook.com", value: "Qsdfghjklm", category: PasswordCategory.perso));
      data.add(Password(id:3, name:"Gmail", url:"www.gmail.com", value: "Poiuytreza", category: PasswordCategory.pro));
      data.add(Password(id:4, name:"Steam", url:"www.steam.com", value: "Mlkjhgfdsq", category: PasswordCategory.gaming));

      passwordList(data);
    } catch (err) {
      print("$err");
    }
  }

  removePassword(int index) {
    this.passwordList.value.removeAt(index);
    update(["Home"]);
  }

  insertPassword(int index, Password p) {
    this.passwordList.value.insert(index, p);
    update(["Home"]);
  }

  addPassword(Password p) {
    p.setId(this.passwordList.value.last.id + 1);
    this.passwordList.value.add(p);
    update(["Home"]);
  }
}